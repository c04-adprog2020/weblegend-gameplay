
package com.c04.weblegends.service;

import com.c04.weblegends.core.aicharacter.*;
import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.spell.Slash;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class BattleServiceTest {
    BattleService battleService;
    PlayerService playerService;

    private boolean listCheck(ArrayList <Object> listObject){
        return listObject.get(0) != null && listObject.get(1) != null && listObject.get(2) != null;
    }
    @BeforeEach
    public void setUp(){
        battleService = new BattleServiceImpl();
        playerService = new PlayerServiceImpl();
    }

    @Test
    public void testSpawnEnemy(){
        ArtificialIntelligence enemy = battleService.spawnEnemy(2);
        assertTrue(enemy instanceof  ArtificialIntelligence);
        assertNotNull(enemy);
    }

    @Test
    public void testPlayerSpell(){
        Character hero = playerService.createHero("nico","knight");
        ArtificialIntelligence enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero = playerService.createHero("nico","cleric");
        enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero = playerService.createHero("nico","assassin");
        enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero = playerService.createHero("nico","archer");
        enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero = playerService.createHero("nico","ranger");
        enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero = playerService.createHero("nico","mage");
        enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
        hero.setSpell(new Slash(30,3));
        assertTrue(listCheck(battleService.playerSpell(hero,enemy)));
    }

    @Test
    public void testPlayerAttack(){
        Character hero = playerService.createHero("nico","knight");
        ArtificialIntelligence enemy = battleService.spawnEnemy(2);
        assertNotNull(battleService.playerAttack(hero,enemy));
    }

    @Test
    public void testEnemyAttack(){
        Character hero = playerService.createHero("nico","mage");
        ArtificialIntelligence enemy = battleService.spawnEnemy(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new ArcherAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new AssassinAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new ClericAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new KnightAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new MageAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
        enemy = new RangerAI(2);
        assertTrue(listCheck(battleService.enemyAttack(enemy,hero)));
    }
}
