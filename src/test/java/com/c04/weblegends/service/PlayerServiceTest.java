
package com.c04.weblegends.service;

import com.c04.weblegends.repo.PlayerRepo;
import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.character.Cleric;
import com.c04.weblegends.core.character.Knight;
import com.c04.weblegends.core.weapon.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerServiceTest {
    PlayerService playerService;
    @BeforeEach
    public void setUp(){
        this.playerService=new PlayerServiceImpl();
    }

    @Test
    public void testPowerUp(){
        Character hero = new Knight("kaguya");
        assertTrue(this.playerService.doPowerUp(hero) instanceof Character);
    }

    @Test
    public void testEnhanceWeapon(){
        Character hero = this.playerService.createHero("kaguya","knight");
        hero=this.playerService.enhanceWeapon(hero);
        assertTrue(hero.getWeapon() instanceof Weapon);
    }

    @Test
    public void testCreateHero(){
        assertTrue(this.playerService.createHero("hayasaka","cleric") instanceof  Cleric);
    }

    @Test
    public void testGetter(){
        assertTrue(this.playerService.getRepo() instanceof  PlayerRepo);
    }

}



