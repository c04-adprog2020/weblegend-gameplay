
package com.c04.weblegends.service;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.character.Knight;
import com.c04.weblegends.core.item.Item;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ItemServiceTest {
    ItemService itemService;
    @BeforeEach
    public void setUp(){
        itemService=new ItemServiceImpl();
    }

    @Test
    public void testGetItem(){
        assertNotNull(this.itemService.getItem());
    }

    @Test
    public void testUseItem(){
        Character hero = new Knight("bakaguya");
        Item item=this.itemService.getItem();
        assertNotNull(this.itemService.useItem(item,hero));
        assertTrue(this.itemService.useItem(item,hero) instanceof  Character);
    }
}
