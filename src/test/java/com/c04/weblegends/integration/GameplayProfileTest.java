package com.c04.weblegends.integration;

import java.time.LocalDateTime;
import com.c04.weblegends.core.aicharacter.ArtificialIntelligence;
import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.runner.RunWith;
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameplayProfileTest {
    String name;
    String className;
    Character player;
    ArtificialIntelligence enemy;
    Item item;
    int counterLoop;
    @Autowired
    BattleService battleService = new BattleServiceImpl();
    @Autowired
    PlayerService playerService = new PlayerServiceImpl();
    @Autowired
    ItemService itemService = new ItemServiceImpl();
    public ArtificialIntelligence getEnemy() {
        return enemy;
    }

    public void delay(int milis) {
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {

        }
    }


    @BeforeEach
    public void setUp(){
        System.out.println("Do setup at "+LocalDateTime.now());
        name="test";
        className="Knight";
        counterLoop=100000000;
        System.out.println("Init hero at "+LocalDateTime.now());
        player=playerService.createHero(name,className);
        System.out.println("Init item at "+LocalDateTime.now());
        item=itemService.getItem();
        System.out.println("Init enemy at "+LocalDateTime.now());
        enemy=battleService.spawnEnemy(3);
        System.out.println("Setup done at "+LocalDateTime.now());
    }
    @Test
    public void testLoad() {
        System.out.println("Do character creation at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(playerService.createHero(name, className));
        }
        System.out.println("Do spawn enemy at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(battleService.spawnEnemy(3));
        }
        System.out.println("Do hero upgrade at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(playerService.doPowerUp(player));
        }
        System.out.println("Do weapon upgrade at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(playerService.enhanceWeapon(player));
        }
        System.out.println("Do spawn item at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(itemService.getItem());
        }
        System.out.println("Do use item at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(itemService.useItem(item, player));
        }
        System.out.println("Do player attack at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(battleService.playerAttack(player, enemy));
        }
        System.out.println("Do player skill at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(battleService.playerSpell(player, enemy));
        }
        System.out.println("Do enemy attack at " + LocalDateTime.now());
        for (int i = 0; i < counterLoop; i++) {
            assertNotNull(battleService.enemyAttack(enemy, player));
        }
        delay(100);
        System.out.println("Load testing done at "+ LocalDateTime.now());
    }
}
