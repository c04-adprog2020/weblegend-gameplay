
package com.c04.weblegends.core.combatLogic;

import com.c04.weblegends.core.combatlogic.*;;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CombatLogicTest {
    @Test
    public void combatLogicTest(){
        CombatLogic combatLogic = new ArcherCombatLogic();
        assertEquals("Attack",combatLogic.regAttack());
        assertEquals("True Strike",combatLogic.useSkill());
        combatLogic = new AssassinCombatLogic();
        assertEquals("Critical Strike",combatLogic.useSkill());
        combatLogic = new ClericCombatLogic();
        assertEquals("Heal",combatLogic.useSkill());
        combatLogic = new KnightCombatLogic();
        assertEquals("Pierce Attack",combatLogic.useSkill());
        combatLogic = new MageCombatLogic();
        assertEquals("Life Drain",combatLogic.useSkill());
        combatLogic = new RangerCombatLogic();
        assertEquals("Multishot",combatLogic.useSkill());
    }
}
