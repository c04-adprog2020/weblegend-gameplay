
package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.character.Knight;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ItemTest {
    Item item;

    @BeforeEach
    public void setUp() {
        item = new Potion();
    }

    @Test
    public void useTest() {
        Character hero = new Knight("W");
        assertTrue(item.use(hero) instanceof  Character);
    }

    @Test
    void getterTest() {
        assertEquals("potion",item.getItemType());
        assertTrue(item.getPercent() > 0);
        item=new Elixir();
        assertEquals("elixir",item.getItemType());
        assertTrue(item.getPercent() > 0);
        Character hero = new Knight("W");
        assertTrue(item.use(hero) instanceof  Character);
    }

}
