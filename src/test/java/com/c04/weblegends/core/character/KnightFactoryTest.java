
package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class KnightFactoryTest {
    CharFactory knightFactory;
    @BeforeEach
    public void setUp(){
        knightFactory=new KnightFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("knight",knightFactory.getType());
    }
    @Test
    public void produceTest(){
        Character chara=knightFactory.produce("w");
        assertEquals("Knight",chara.getClassName());
        assertEquals("w",chara.getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Sword);
        //assertTrue(char.getSkill() instanceof  Crit);
    }

}
