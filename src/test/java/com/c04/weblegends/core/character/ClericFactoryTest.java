
package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ClericFactoryTest {
    CharFactory clericFactory;
    @BeforeEach
    public void setUp(){
        clericFactory=new ClericFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("cleric",clericFactory.getType());
    }
    @Test
    public void produceTest(){
        Character chara=clericFactory.produce("w");
        assertEquals("Cleric",chara.getClassName());
        assertEquals("w",chara.getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Staff);
        //assertTrue(char.getSkill() instanceof  Heal);
    }

}
