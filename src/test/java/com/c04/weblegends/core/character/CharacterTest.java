
package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CharacterTest {
    Character arthur;

    @BeforeEach
    public void setUp() {
        arthur = new Knight("Apollonia");
    }

    @Test
    public void testGetter() {
        assertEquals(2000, arthur.getMaxHP());
        assertEquals(500, arthur.getMaxMana());
        assertEquals("Apollonia", arthur.getName());
        assertEquals("Knight", arthur.getClassName());
        arthur.setWeapon(new Sword(300,10,"Murasama"));
        arthur.setSpell(new TrueStrike(30,4));
        assertTrue(arthur.getWeapon() instanceof  Sword);
        assertEquals(300,arthur.getDamage());
        assertEquals(10,arthur.getDefense());
        assertNotNull(arthur.getSpell());

    }

    @Test
    public void testSetter() {
        arthur.setMana(40);
        arthur.setHP(100);
        assertEquals(100, arthur.getHP());
        assertEquals(40, arthur.getMana());
        arthur.setHP(10000);
        arthur.setMana(10000);
        assertEquals(2000, arthur.getHP());
        assertEquals(500, arthur.getMana());
        arthur.setHP(-2);
        arthur.setMana(-3);
        assertEquals(0, arthur.getHP());
        assertEquals(0, arthur.getMana());
    }

    @Test
    public void useSpell(){
        arthur.setSpell(new TrueStrike(30,1));
        assertTrue(arthur.useSpell());
        arthur.setSpell(new Slash(300000,1));
        assertFalse(arthur.useSpell());
    }
}
