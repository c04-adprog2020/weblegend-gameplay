
package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ArcherFactoryTest {
    CharFactory archerFactory;
    @BeforeEach
    public void setUp(){
        archerFactory=new ArcherFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("archer",archerFactory.getType());
    }
    @Test
    public void produceTest(){
        Character chara=archerFactory.produce("w");
        assertEquals("Archer",chara.getClassName());
        assertEquals("w",chara.getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Bow);
        //assertTrue(char.getSkill() instanceof  Focus);
    }

}
