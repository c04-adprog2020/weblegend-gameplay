

package com.c04.weblegends.controller;

import com.c04.weblegends.service.BattleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers={BattleAPIController.class})
public class BattleAPITest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BattleService battleService;

    private static String generateJson2() {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ObjectNode weaponNode = mapper.createObjectNode();
        ObjectNode spellNode = mapper.createObjectNode();
        ObjectNode playerNode = mapper.createObjectNode();
        ObjectNode enemyNode = mapper.createObjectNode();
        //Weapon Node
        weaponNode.put("type", "Sword");
        weaponNode.put("attack", 300);
        weaponNode.put("defense", 100);
        weaponNode.put("name", "Knight Broadsword");
        weaponNode.put("weaponType", "sword");
        //Spell Node
        spellNode.put("type", "TrueStrike");
        spellNode.put("mana", 200);
        spellNode.put("cooldown", 3);
        spellNode.put("name", "true strike");
        spellNode.put("cooldownCounter", 0);
        //Player Node
        playerNode.put("type", "Knight");
        playerNode.put("name", "Nico");
        playerNode.put("maxHP", 2000);
        playerNode.put("maxMana", 500);
        playerNode.put("weapon", weaponNode);
        playerNode.put("hp", 2000);
        playerNode.put("mana", 500);
        playerNode.put("spell", spellNode);
        playerNode.put("className", "Knight");
        playerNode.put("defense", 100);
        playerNode.put("damage", 300);
        //Enemy Node
        enemyNode.put("type", "KnightAI");
        enemyNode.put("level", 2);
        enemyNode.put("maxHP", 2330);
        enemyNode.put("skillCooldown", 2);
        enemyNode.put("evasionRate", 0.5);
        enemyNode.put("accuracy", 74);
        enemyNode.put("currentMove", "Pierce Attack");
        enemyNode.put("moveDescription", "This sword pierce throught anything");
        enemyNode.put("defense", 18);
        enemyNode.put("damage", 37);
        enemyNode.put("hp", 1448);
        //Root Node
        rootNode.put("player", playerNode);
        rootNode.put("enemy", enemyNode);
        return rootNode.toString();
    }

    @Test
    public void playerAttackTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/battle-api/player-attack")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson2())).andExpect(status().isBadRequest());
        mockMvc.perform(post("/battle-api/player-attack")).andExpect(status().isBadRequest());
    }

    @Test
    public void enemyAttackTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/battle-api/enemy-attack")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson2())).andExpect(status().isBadRequest());
        mockMvc.perform(post("/battle-api/enemy-attack")).andExpect(status().isBadRequest());
    }

    @Test
    public void playerSkillTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/battle-api/player-skill")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson2())).andExpect(status().isBadRequest());
        mockMvc.perform(post("/battle-api/player-skill")).andExpect(status().isBadRequest());
    }

    @Test
    public void spawnEnemyTest() throws Exception{
        mockMvc.perform(get("/battle-api/spawn-enemy?level=2")).andExpect(status().isCreated());
    }


}
