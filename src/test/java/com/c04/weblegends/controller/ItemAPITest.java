
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.c04.weblegends.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(controllers = {ItemAPIController.class})
public class ItemAPITest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ItemService itemService;

    private static String generateJson(){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ObjectNode itemNode = mapper.createObjectNode();
        ObjectNode characterNode = mapper.createObjectNode();
        itemNode.put("type","Potion");
        itemNode.put("itemType","potion");
        itemNode.put("percent",0.5);
        characterNode.put("type","Knight");
        characterNode.put("name","Nico");
        characterNode.put("maxHP",2000);
        characterNode.put("MaxMana",1403);
        characterNode.put("hp",500);
        characterNode.put("mana",500);
        characterNode.put("className","Knight");
        rootNode.put("item",itemNode);
        rootNode.put("character",characterNode);
        return rootNode.toString();
    }

    @Test
    public void whenGetItemCalledItShouldReturnJSON() throws Exception{
        mockMvc.perform(get("/item-api/drop-item")).andExpect(status().isCreated());
        System.out.println(generateJson());
    }


    @Test
    public void useTimeTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/item-api/use-item")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson())).andExpect(status().isOk());
        mockMvc.perform(post("/item-api/use-item")).andExpect(status().isBadRequest());
    }
}
