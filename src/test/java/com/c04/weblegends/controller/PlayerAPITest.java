
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.c04.weblegends.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(controllers = {PlayerAPIController.class})

public class PlayerAPITest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;

    public static String generateJson(){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode characterNode = mapper.createObjectNode();
        ObjectNode weaponNode = mapper.createObjectNode();
        weaponNode.put("type","Sword");
        weaponNode.put("attack",300);
        weaponNode.put("defense",100);
        weaponNode.put("name","Knight Broadsword");
        weaponNode.put("weaponType","sword");
        characterNode.put("type","Knight");
        characterNode.put("name","Nico");
        characterNode.put("maxHP",2000);
        characterNode.put("MaxMana",1403);
        characterNode.put("weapon",weaponNode);
        characterNode.put("hp",500);
        characterNode.put("mana",500);
        characterNode.put("className","Knight");
        return characterNode.toString();
    }

    @Test
    public void upgradeHeroTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/player-api/upgrade-hero")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson())).andExpect(status().isOk());
    }

    @Test
    public void enhanceWeaponTest() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/player-api/enhance-weapon")
                .contentType(MediaType.APPLICATION_JSON)
                .content(generateJson())).andExpect(status().isOk());
    }

    @Test
    public void createHeroTest() throws  Exception{
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.put("name","bakaguya");
        rootNode.put("className","knight");
        String jsonString=rootNode.toString();
        mockMvc.perform(MockMvcRequestBuilders.post("/player-api/produce-hero")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)).andExpect(status().isCreated());
    }

    @Test
    public void testGetClass() throws  Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/player-api/get-heroclass")).andExpect(status().isOk());
    }
}
