package com.c04.weblegends.service;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.repo.PlayerRepo;

import java.util.Map;

public interface PlayerService {
    public Character doPowerUp(Character character);

    public Character createHero(String name, String type);

    public PlayerRepo getRepo();

    public Character enhanceWeapon(Character character);

    public Map<String,String> getHeroClass();
}
