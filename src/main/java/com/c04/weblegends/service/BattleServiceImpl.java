package com.c04.weblegends.service;

import com.c04.weblegends.core.aicharacter.*;
import com.c04.weblegends.core.character.Character;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Random;

@Service
public class BattleServiceImpl implements BattleService {
    /**
     * Implement player attack to enemy.
     *
     * @param player Player Object
     * @param enemy  Enemy object
     * @return List that contains enemy & player stat + battle log
     */
    @Override
    public ArrayList<Object> playerAttack(Character player, ArtificialIntelligence enemy) {
        ArrayList<Object> output = new ArrayList<Object>();
        float chance = new Random().nextFloat();
        if (chance > enemy.getEvasionRate()) {
            int totalDamage = player.getDamage() - enemy.getDefense();
            if (totalDamage < 0) {
                totalDamage = 0;
            }
            enemy.setHP(enemy.getHP() - totalDamage);
            player.decCooldown();
            output.add(player);
            output.add(enemy);
            output.add(String.format("Player Turn : Player dealt %d damage to enemy!", totalDamage));
            return output;
        }
        player.decCooldown();
        output.add(player);
        output.add(enemy);
        output.add("Player Turn : Enemy successfully dodged the attack!");
        return output;
    }

    /**
     * Implement player spell to enemy.
     *
     * @param player Player object
     * @param enemy  AI object
     * @return Player & Enemy stat + battle log
     */
    public ArrayList<Object> playerSpell(Character player, ArtificialIntelligence enemy) {
        ArrayList<Object> output = new ArrayList<Object>();
        if (player.useSpell()) {
            int totalDamage;
            switch (player.getSpell().getName()) {
                case "slash":
                    totalDamage = (player.getDamage() * 2 - enemy.getDefense() >= 0) ? player.getDamage() * 2
                            - enemy.getDefense() : 0;
                    enemy.setHP(enemy.getHP() - totalDamage);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "heal":
                    player.setHP(player.getHP() * 145 / 100);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "multishot":
                    totalDamage = 0;
                    Random r = new Random();
                    int multiHit = r.nextInt(3) + 2;
                    for (int itr = 0; itr < multiHit; itr++) {
                        float chance = new Random().nextFloat();
                        if (chance > enemy.getEvasionRate()) {
                            totalDamage += enemy.getDamage();
                        }
                    }
                    totalDamage = (totalDamage - enemy.getDefense() >= 0) ? totalDamage - enemy.getDefense() : 0;
                    enemy.setHP(enemy.getHP() - totalDamage);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "true strike":
                    totalDamage = (player.getDamage() * 3 - enemy.getDefense() >= 0) ? player.getDamage() * 3 - enemy.getDefense() : 0;
                    enemy.setHP(enemy.getHP() - totalDamage);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "backstab":
                    totalDamage = (player.getDamage() * 3 / 2 - enemy.getDefense() >= 0) ? player.getDamage() * 3 / 2 - enemy.getDefense() : 0;
                    enemy.setHP(enemy.getHP() - totalDamage);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "nova":
                    totalDamage = (player.getDamage() * 5 - enemy.getDefense() >= 0) ? player.getDamage() * 5 - enemy.getDefense() : 0;
                    enemy.setHP(enemy.getHP() - totalDamage);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;

                case "snipe":
                    enemy.setHP(enemy.getHP() * 75 / 100);
                    output.add(player);
                    output.add(enemy);
                    output.add(String.format("Player Turn : Player use %s !", player.getSpell().getName()));
                    return output;
            }
        }
        return this.playerAttack(player, enemy);
    }

    /**
     * Implement enemy attack to player.
     *
     * @param enemy  Enemy object
     * @param player Player Object
     * @return List that contains enemy & player stat + battle log
     */
    @Override
    public ArrayList<Object> enemyAttack(ArtificialIntelligence enemy, Character player) {
        Random r = new Random();
        ArrayList<Object> output = new ArrayList<Object>();
        int randomValue;
        String result;
        enemy.executeMove();
        switch (enemy.getCurrentMove()) {
            case "Attack":
                enemy.decreaseCooldown();
                randomValue = r.nextInt(100);
                if (randomValue < enemy.getAccuracy()) {
                    int totalDamage = enemy.getDamage() - player.getDefense();
                    if (totalDamage < 0) {
                        totalDamage = 0;
                    }
                    int updatedHP = player.getHP()-totalDamage;
                    player.setHP(updatedHP);
                    result = "Enemy Turn : " + enemy.getMoveDescription();
                    result += " Enemy dealt " + totalDamage;
                    result += " damage to player!";
                    output.add(player);
                    output.add(enemy);
                    output.add(result);
                    return output;
                }
                output.add(player);
                output.add(enemy);
                output.add("Enemy Turn : Player successfully dodged the attack!");
                return output;

            case "True Strike":
                enemy.setSkillCooldown(4);
                int totalDamage = enemy.getDamage() - player.getDefense();
                if (totalDamage < 0) {
                    totalDamage = 0;
                }
                player.setHP(player.getHP() - totalDamage);
                result = "Enemy Turn : " + enemy.getMoveDescription();
                result += " Enemy dealt " + totalDamage;
                result += " damage to player!";
                output.add(player);
                output.add(enemy);
                output.add(result);
                return output;

            case "Multishot":
                enemy.setSkillCooldown(4);
                totalDamage = 0;
                for (int itr = 0; itr < 3; itr++) {
                    randomValue = r.nextInt(100);
                    if (randomValue < enemy.getAccuracy()) {
                        totalDamage += enemy.getDamage();
                    }
                }
                totalDamage = totalDamage - player.getDefense();
                if (totalDamage < 0) {
                    totalDamage = 0;
                }
                player.setHP(player.getHP() - totalDamage);
                result = "Enemy Turn : " + enemy.getMoveDescription();
                result += " Enemy dealt " + totalDamage;
                result += " damage to player!";
                output.add(player);
                output.add(enemy);
                output.add(result);
                return output;

            case "Critical Strike":
                enemy.setSkillCooldown(4);
                randomValue = r.nextInt(100);
                totalDamage = 2 * enemy.getDamage() - player.getDefense();
                if (totalDamage < 0) {
                    totalDamage = 0;
                }
                if (randomValue < enemy.getAccuracy()) {
                    int updatedHP = player.getHP() - totalDamage;
                    player.setHP(updatedHP);
                    result = "Enemy Turn : " + enemy.getMoveDescription();
                    result += " Enemy dealt " + totalDamage;
                    result += " damage to player!";
                    output.add(player);
                    output.add(enemy);
                    output.add(result);
                    return output;
                }
                output.add(player);
                output.add(enemy);
                output.add("Enemy Turn : Player successfully dodged the attack!");
                return output;

            case "Heal":
                enemy.setSkillCooldown(4);
                enemy.setHP(enemy.getHP() + enemy.getDamage());
                result = "Enemy Turn : " + enemy.getMoveDescription();
                result += " Enemy heal " + enemy.getDamage();
                result += " hit point!";
                output.add(player);
                output.add(enemy);
                output.add(result);
                return output;

            case "Pierce Attack":
                enemy.setSkillCooldown(4);
                randomValue = r.nextInt(100);
                if (randomValue < enemy.getAccuracy()) {
                    totalDamage = 20 + enemy.getDamage() - player.getDefense();
                    if (totalDamage < 0) {
                        totalDamage = 0;
                    }
                    int updatedHP = player.getHP() - totalDamage;
                    player.setHP(updatedHP);
                    result = "Enemy Turn : " + enemy.getMoveDescription();
                    result += " Enemy dealt " + totalDamage;
                    result += " damage to player!";
                    output.add(player);
                    output.add(enemy);
                    output.add(result);
                    return output;
                }
                output.add(player);
                output.add(enemy);
                output.add("Enemy Turn : Player successfully dodged the attack!");
                return output;

            case "Life Drain":
                enemy.setSkillCooldown(4);
                player.setHP(player.getHP() - enemy.getDamage());
                enemy.setHP(enemy.getHP() + enemy.getDamage());
                result = "Enemy Turn : " + enemy.getMoveDescription();
                result += " Enemy drain " + enemy.getDamage();
                result += " hit point from player!";
                output.add(player);
                output.add(enemy);
                output.add(result);
                return output;
            default:
                output.add(player);
                output.add(enemy);
                output.add("Enemy Turn : Enemy doesn't make a move");
                return output;
        }
    }

    /**
     * Spawn enemy on certain level randomly.
     *
     * @param level Level of the enemy
     * @return Enemy object
     */
    @Override
    public ArtificialIntelligence spawnEnemy(int level) {
        Random r = new Random();
        ArtificialIntelligence enemy;
        int randomValue = r.nextInt(6);
        switch (randomValue) {
            case 0:
                enemy = new ArcherAI(level);
                break;
            case 1:
                enemy = new AssassinAI(level);
                break;
            case 2:
                enemy = new ClericAI(level);
                break;
            case 3:
                enemy = new KnightAI(level);
                break;
            case 4:
                enemy = new MageAI(level);
                break;
            case 5:
                enemy = new RangerAI(level);
                break;
            default:
                enemy = new ArcherAI(level);
        }
        return enemy;
    }
}
