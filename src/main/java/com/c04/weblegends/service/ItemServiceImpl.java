package com.c04.weblegends.service;

import com.c04.weblegends.core.item.*;
import com.c04.weblegends.core.character.Character;
import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements ItemService {
    /**
     * Item random drop service.
     *
     * @return Random item
     */
    public Item getItem() {
        Item item = null;
        int randomChoice = new Random().nextInt(2);
        switch (randomChoice) {
            case 0:
                item = new Potion();
                break;
            case 1:
                item = new Elixir();
                break;
        }
        return item;
    }

    /**
     * Apply item to certain player service.
     *
     * @param item      Item that will be used
     * @param character Player/Hero object that is being applied into
     * @return Player/Hero after item appliance.
     */
    public Character useItem(Item item, Character character) {
        return item.use(character);
    }
}
