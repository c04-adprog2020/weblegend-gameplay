package com.c04.weblegends.service;

import com.c04.weblegends.core.character.CharFactory;
import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.weapon.Weapon;
import com.c04.weblegends.repo.PlayerRepo;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PlayerServiceImpl implements PlayerService {
    private Character player;
    private PlayerRepo playerRepo;

    /**
     * Initial setup (Could be changed in the future).
     */
    public PlayerServiceImpl() {
        playerRepo = new PlayerRepo();
    }

    /**
     * Method to implement powerup on player.
     *
     * @return : Upgraded player
     */
    @Override
    public Character doPowerUp(Character character) {
        character.doUpgrade();
        return character;
    }

    /**
     * Hero creation based on class.
     *
     * @param name : Name of the hero
     * @param type : Class of the hero
     * @throws NullPointerException : Exception if there is null kind of className
     */
    @Override
    public Character createHero(String name, String type) {
        CharFactory charFactory = playerRepo.getFactoryByName(type);
        this.player = charFactory.produce(name);
        return charFactory.produce(name);
    }

    @Override
    public PlayerRepo getRepo() {
        return this.playerRepo;
    }

    /**
     * Weapon enhancemenet service.
     *
     * @param character Character object
     * @return Character that has upgraded weapon
     */
    @Override
    public Character enhanceWeapon(Character character) {
        Weapon heroWeapon = character.getWeapon();
        heroWeapon.doUpgrade();
        character.setWeapon(heroWeapon);
        return character;
    }

    @Override
    public Map <String,String> getHeroClass(){
        return this.playerRepo.getHeroClasses();
    }

}
