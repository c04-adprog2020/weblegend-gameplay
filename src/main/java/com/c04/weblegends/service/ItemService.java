package com.c04.weblegends.service;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.item.Item;
import org.springframework.stereotype.Service;

@Service
public interface ItemService {
    public Item getItem();

    public Character useItem(Item item, Character character);
}
