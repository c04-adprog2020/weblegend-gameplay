package com.c04.weblegends.service;

import com.c04.weblegends.core.aicharacter.ArtificialIntelligence;
import com.c04.weblegends.core.character.Character;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public interface BattleService {
    public ArrayList<Object> playerAttack(Character player, ArtificialIntelligence enemy);

    public ArrayList<Object> playerSpell(Character player, ArtificialIntelligence enemy);

    public ArrayList<Object> enemyAttack(ArtificialIntelligence enemy, Character player);

    public ArtificialIntelligence spawnEnemy(int level);
}
