package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ArcherAI.class, name = "ArcherAI"),
        @JsonSubTypes.Type(value = AssassinAI.class, name = "AssassinAI"),
        @JsonSubTypes.Type(value = ClericAI.class, name = "ClericAI"),
        @JsonSubTypes.Type(value = KnightAI.class, name = "KnightAI"),
        @JsonSubTypes.Type(value = MageAI.class, name = "MageAI"),
        @JsonSubTypes.Type(value = RangerAI.class, name = "RangerAI")
})
public abstract class ArtificialIntelligence {
    protected int maxHP;
    protected int currentHP;
    protected int skillCooldown;
    protected int level;
    protected int atkPoint;
    protected int defPoint;
    protected int critRate;
    protected float evasionRate;
    protected int accuracy;
    protected CombatLogic combatLogic;
    protected String characterClass;
    protected String currentMove;
    protected String moveDescription;

    public int getMaxHP() {
        return this.maxHP;
    }

    public int getHP() {
        return this.currentHP;
    }

    public int getDefense() {
        return this.defPoint;
    }

    public int getSkillCooldown() {
        return this.skillCooldown;
    }

    public int getDamage() {
        return this.atkPoint;
    }

    public int getLevel() {
        return this.level;
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public String getCurrentMove() {
        return this.currentMove;
    }

    public String getMoveDescription() {
        return this.moveDescription;
    }

    public float getEvasionRate() {
        return this.evasionRate;
    }

    public void setSkillCooldown(int cooldown) {
        this.skillCooldown = cooldown;
    }

    /**
     * Method to decrease the skill cooldown.
     */
    public void decreaseCooldown() {
        this.skillCooldown--;
        if (this.skillCooldown < 0) {
            this.skillCooldown = 0;
        }
    }

    public abstract void executeMove();

    /**
     * Method to set hp.
     *
     * @param hp : hp value
     */
    public void setHP(int hp) {
        if (hp >= this.maxHP) {
            this.currentHP = this.maxHP;
        } else if (hp <= 0) {
            this.currentHP = 0;
        } else {
            this.currentHP = hp;
        }
    }

    @Override
    public String toString() {
        return "Level " + this.level + " " + this.characterClass;
    }
}