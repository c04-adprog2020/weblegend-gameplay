package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RangerAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public RangerAI(@JsonProperty("level") int level) {
        this.maxHP = 2000 + 125 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 26 + 7 * level;
        this.defPoint = 7 + 1 * level;
        this.critRate = 10 + 3 * level;
        this.accuracy = 72 + 3 * level;
        this.combatLogic = new RangerCombatLogic();
        this.characterClass = "Ranger";
        this.evasionRate = 0.5f;
    }

    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Shotgun";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "Triple barrel at a time";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}