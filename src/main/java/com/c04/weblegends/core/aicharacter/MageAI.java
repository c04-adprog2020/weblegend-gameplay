package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MageAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public MageAI(@JsonProperty("level") int level) {
        this.maxHP = 1800 + 120 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 35 + 7 * level;
        this.defPoint = 11 + 1 * level;
        this.critRate = 14 + 2 * level;
        this.accuracy = 77 + 2 * level;
        this.combatLogic = new MageCombatLogic();
        this.characterClass = "Mage";
        this.evasionRate = 0.4f;
    }

    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Staff";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "I will drain your life";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}