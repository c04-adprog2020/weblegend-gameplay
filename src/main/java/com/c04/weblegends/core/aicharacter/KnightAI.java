package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class KnightAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public KnightAI(@JsonProperty("level") int level) {
        this.maxHP = 2050 + 140 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 25 + 6 * level;
        this.defPoint = 10 + 4 * level;
        this.critRate = 10 + 2 * level;
        this.accuracy = 70 + 2 * level;
        this.combatLogic = new KnightCombatLogic();
        this.characterClass = "Knight";
        this.evasionRate = 0.5f;
    }

    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Sword";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "This sword pierce throught anything";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}