package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Random;

public class AssassinAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public AssassinAI(@JsonProperty("level") int level) {
        this.maxHP = 1800 + 105 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 27 + 9 * level;
        this.defPoint = 5 + 3 * level;
        this.critRate = 13 + 3 * level;
        this.accuracy = 75 + 3 * level;
        this.combatLogic = new AssassinCombatLogic();
        this.characterClass = "Assassin";
        this.evasionRate = 0.6f;
    }

    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Dagger";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "Feel the deep wound";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}