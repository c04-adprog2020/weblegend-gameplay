package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClericAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public ClericAI(@JsonProperty("level") int level) {
        this.maxHP = 2100 + 145 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 22 + 6 * level;
        this.defPoint = 10 + 4 * level;
        this.critRate = 8 + 2 * level;
        this.accuracy = 68 + 3 * level;
        this.combatLogic = new ClericCombatLogic();
        this.characterClass = "Cleric";
        this.evasionRate = 0.4f;
    }

    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Mace";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "Healing for survival";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}