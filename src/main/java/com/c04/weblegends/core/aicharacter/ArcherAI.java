package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.combatlogic.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ArcherAI extends ArtificialIntelligence {
    /**
     * Class constructor.
     *
     * @param level Level of enemy
     */
    @JsonCreator
    public ArcherAI(@JsonProperty("level") int level) {
        this.maxHP = 2000 + 120 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 25 + 7 * level;
        this.defPoint = 7 + 2 * level;
        this.critRate = 11 + 3 * level;
        this.accuracy = 70 + 3 * level;
        this.combatLogic = new ArcherCombatLogic();
        this.characterClass = "Archer";
        this.evasionRate = 0.5f;
    }


    @Override
    public void executeMove() {
        if (this.skillCooldown != 0) {
            this.moveDescription = "Attack with my Bow";
            this.currentMove = this.combatLogic.regAttack();
        } else {
            this.moveDescription = "My attack never miss";
            this.currentMove = this.combatLogic.useSkill();
        }
    }
}