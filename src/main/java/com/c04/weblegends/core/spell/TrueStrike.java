package com.c04.weblegends.core.spell;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TrueStrike extends Spell {
    @JsonCreator
    public TrueStrike(@JsonProperty ("mana") int mana,@JsonProperty ("cooldown") int cooldown){
        this.mana=mana;
        this.cooldown=cooldown;
        this.cooldownCounter=0;
    }
    public String getName(){
        return "true strike";
    }
}
