package com.c04.weblegends.core.spell;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Backstab.class, name = "Backstab"),
        @JsonSubTypes.Type(value = Heal.class, name = "Heal"),
        @JsonSubTypes.Type(value = Multishot.class, name = "Multishot"),
        @JsonSubTypes.Type(value = Nova.class, name = "Nova"),
        @JsonSubTypes.Type(value = Slash.class, name = "Snipe"),
        @JsonSubTypes.Type(value = TrueStrike.class, name = "TrueStrike")
})
public abstract class Spell {
        protected String name;
        protected int mana;
        protected int cooldown;
        protected int cooldownCounter;
        public  abstract String getName();
        public int getMana(){
            return this.mana;
        }

        public int getCooldownCounter(){
            return this.cooldownCounter;
        }

        public void setCooldownCounter(int cooldown){
            this.cooldownCounter=cooldown;
        }

        public int getCooldown(){
            return this.cooldown;
        }
}
