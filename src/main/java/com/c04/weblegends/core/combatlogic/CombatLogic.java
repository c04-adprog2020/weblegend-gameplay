package com.c04.weblegends.core.combatlogic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ArcherCombatLogic.class, name = "ArcherCombatLogic"),
        @JsonSubTypes.Type(value = AssassinCombatLogic.class, name = "AssassinCombatLogic"),
        @JsonSubTypes.Type(value = ClericCombatLogic.class, name = "ClericCombatLogic"),
        @JsonSubTypes.Type(value = KnightCombatLogic.class, name = "KnightCombatLogic"),
        @JsonSubTypes.Type(value = MageCombatLogic.class, name = "MageCombatLogic"),
        @JsonSubTypes.Type(value = RangerCombatLogic.class, name = "RangerCombatLogic")
})
public abstract class CombatLogic {
    public abstract String useSkill();

    public String regAttack(){
        return "Attack";
    }
}