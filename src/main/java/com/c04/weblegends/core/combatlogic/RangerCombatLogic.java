package com.c04.weblegends.core.combatlogic;

public class RangerCombatLogic extends CombatLogic {
    @Override
    public String useSkill() {
        return "Multishot";
    }
}