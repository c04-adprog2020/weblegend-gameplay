package com.c04.weblegends.core.combatlogic;

public class AssassinCombatLogic extends CombatLogic {
    @Override
    public String useSkill() {
        return "Critical Strike";
    }

}