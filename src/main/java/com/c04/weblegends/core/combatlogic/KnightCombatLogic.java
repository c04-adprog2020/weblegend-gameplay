package com.c04.weblegends.core.combatlogic;

public class KnightCombatLogic extends CombatLogic {
    @Override
    public String useSkill() {
        return "Pierce Attack";
    }
}