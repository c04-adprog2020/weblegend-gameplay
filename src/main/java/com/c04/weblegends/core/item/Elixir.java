package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.Character;

public class Elixir extends Item {
    /**
     * Apply elixir to restore hero/player MP.
     *
     * @param character Player object
     * @return Player object after applying elixir
     */
    public Character use(Character character) {
        character.setMana((int) Math.round((1 + this.percentage) * (float) character.getMana()));
        return character;
    }

    @Override
    public String getItemType() {
        return "elixir";
    }

    @Override
    public float getPercent() {
        return this.percentage;
    }
}
