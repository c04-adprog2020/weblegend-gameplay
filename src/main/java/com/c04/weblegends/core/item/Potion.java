package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.Character;

public class Potion extends Item {

    /**
     * Apply potion to restore hero/player HP.
     *
     * @param character Player object
     * @return Player object after applying potion
     */
    public Character use(Character character) {
        character.setHP((int) Math.round((1 + this.percentage) * (float) character.getHP()));
        return character;
    }

    @Override
    public String getItemType() {
        return "potion";
    }

    @Override
    public float getPercent() {
        return this.percentage;
    }

}
