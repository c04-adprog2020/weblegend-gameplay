package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.Character;
import com.fasterxml.jackson.annotation.*;
import java.util.Random;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Elixir.class, name = "Elixir"),
        @JsonSubTypes.Type(value = Potion.class, name = "Potion"),
})
public abstract class Item {
    protected float percentage;

    /**
     * Item constructor.
     */
    @JsonCreator
    public Item() {
        this.percentage = new Random().nextFloat() + 1;
    }

    public abstract Character use(Character character);

    public abstract String getItemType();

    public abstract float getPercent();
}
