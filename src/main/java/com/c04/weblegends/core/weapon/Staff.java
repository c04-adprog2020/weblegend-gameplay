package com.c04.weblegends.core.weapon;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Staff extends Weapon {
    /**
     * Constructor for staff weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    @JsonCreator
    public Staff(@JsonProperty("attack") int atk,
                 @JsonProperty("defense") int def, @JsonProperty("name") String name) {
        super(atk, def, name);
    }

    @Override
    public String getWeaponType() {
        return "staff";
    }
}
