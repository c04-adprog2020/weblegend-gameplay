package com.c04.weblegends.core.weapon;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Dagger extends Weapon {
    /**
     * Constructor for dagger weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    @JsonCreator
    public Dagger(@JsonProperty("attack") int atk,
                  @JsonProperty("defense") int def, @JsonProperty("name") String name) {
        super(atk, def, name);
    }

    @Override
    public String getWeaponType() {
        return "dagger";
    }
}
