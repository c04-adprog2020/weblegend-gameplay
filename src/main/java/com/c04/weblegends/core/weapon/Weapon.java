package com.c04.weblegends.core.weapon;

import com.c04.weblegends.core.character.Character;
import com.fasterxml.jackson.annotation.*;
import java.util.Random;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Bow.class, name = "Bow"),
        @JsonSubTypes.Type(value = Crossbow.class, name = "Crossbow"),
        @JsonSubTypes.Type(value = Dagger.class, name = "Dagger"),
        @JsonSubTypes.Type(value = Staff.class, name = "Staff"),
        @JsonSubTypes.Type(value = Sword.class, name = "Sword")
})
public abstract class Weapon {
    protected int defPoint;
    protected int atkPoint;
    protected String name;

    /**
     * Constructor Weapon.
     *
     * @param atkPoint Damage value of weapon
     * @param defPoint Defense value of weapon
     * @param name     Name of the weapon
     */
    @JsonCreator
    public Weapon(@JsonProperty("attack") int atkPoint,
                  @JsonProperty("defense") int defPoint, @JsonProperty("name") String name) {
        this.defPoint = defPoint;
        this.atkPoint = atkPoint;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getAttack() {
        return this.atkPoint;
    }

    public int getDefense() {
        return this.defPoint;
    }

    public abstract String getWeaponType();

    /**
     * Do enchancement on weapon.
     */
    public void doUpgrade() {
        Random r = new Random();
        float percent = r.nextFloat() + 1;
        int upgradeChoice = r.nextInt(2);
        switch (upgradeChoice) {
            case 0:
                this.atkPoint = (int) Math.round(((float) 1 + percent) * (float) this.atkPoint);
                break;
            case 1:
                this.defPoint = (int) Math.round(((float) 1 + percent) * (float) this.defPoint);
                break;
        }
    }

}