package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.Snipe;
import com.c04.weblegends.core.weapon.*;

public class RangerFactory implements CharFactory {
    /**
     * Produce ranger class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Ranger(name);
        chara.setWeapon(new Crossbow(80, 8, "Ranger Crossbow"));
        chara.setSpell(new Snipe(250,3));
        return chara;
    }

    @Override
    public String getType() {
        return "ranger";
    }
}
