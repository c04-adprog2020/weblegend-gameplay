package com.c04.weblegends.core.character;

public interface CharFactory {
    public Character produce(String name);

    public String getType();
}
