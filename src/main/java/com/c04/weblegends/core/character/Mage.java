package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Mage extends Character {
    /**
     * Constructor for mage class.
     *
     * @param name : Name of player
     */
    @JsonCreator
    public Mage(@JsonProperty("name")String name) {
        super(name, 1000, 2500);
    }

    @Override
    public String getClassName() {
        return "Mage";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }


}