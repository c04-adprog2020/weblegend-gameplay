package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Knight extends Character {
    /**
     * Constructor for knight class.
     *
     * @param name : Name of player
     */
    @JsonCreator
    public Knight(@JsonProperty("name")String name) {
        super(name, 2000, 500);
    }

    @Override
    public String getClassName() {
        return "Knight";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }


}