package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.TrueStrike;
import com.c04.weblegends.core.weapon.*;

public class KnightFactory implements CharFactory {
    /**
     * Produce knight class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Knight(name);
        chara.setWeapon(new Sword(300, 100, "Knight Broadsword"));
        chara.setSpell(new TrueStrike(200,3));
        return chara;
    }

    @Override
    public String getType() {
        return "knight";
    }
}
