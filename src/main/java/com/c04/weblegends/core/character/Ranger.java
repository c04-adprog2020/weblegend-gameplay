package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Ranger extends Character {
    /**
     * Constructor for ranger class.
     *
     * @param name : Name of player
     */
    @JsonCreator
    public Ranger(@JsonProperty("name")String name) {
        super(name, 1000, 500);
    }

    @Override
    public String getClassName() {
        return "Ranger";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */

    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

}