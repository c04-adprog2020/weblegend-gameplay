package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.Nova;
import com.c04.weblegends.core.weapon.*;

public class MageFactory implements CharFactory {
    /**
     * Produce mage class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Mage(name);
        chara.setWeapon(new Staff(25, 0, "Mage Staff"));
        chara.setSpell(new Nova(500,3));
        return chara;
    }

    @Override
    public String getType() {
        return "mage";
    }
}
