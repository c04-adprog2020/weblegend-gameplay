package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.Heal;
import com.c04.weblegends.core.weapon.*;

public class ClericFactory implements CharFactory {
    /**
     * Produce cleric class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Cleric(name);
        chara.setWeapon(new Staff(12, 0, "Cleric Staff"));
        chara.setSpell(new Heal(400,3));
        return chara;
    }

    public String getType() {
        return "cleric";
    }
}
