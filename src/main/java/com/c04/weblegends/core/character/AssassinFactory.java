package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.Backstab;
import com.c04.weblegends.core.weapon.*;

public class AssassinFactory implements CharFactory {
    /**
     * Produce assassin class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Assassin(name);
        chara.setWeapon(new Dagger(75, 25, "Assassin Dagger"));
        chara.setSpell(new Backstab(100,3));
        return chara;
    }

    @Override
    public String getType() {
        return "assassin";
    }
}
