package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Assassin extends Character {
    /**
     * Constructor for assassin class.
     *
     * @param name : Name of player
     */
    @JsonCreator
    public Assassin(@JsonProperty("name")String name) {
        super(name, 500, 200);
    }

    @Override
    public String getClassName() {
        return "Assassin";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

}