package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Cleric extends Character {
    /**
     * Constructor for Cleric class.
     *
     * @param name : Name of player
     */
    @JsonCreator
    public Cleric(@JsonProperty("name")String name) {
        super(name, 1500, 2000);
    }

    @Override
    public String getClassName() {
        return "Cleric";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

}