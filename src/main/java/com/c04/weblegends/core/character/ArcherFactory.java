package com.c04.weblegends.core.character;

import com.c04.weblegends.core.spell.Multishot;
import com.c04.weblegends.core.weapon.*;

public class ArcherFactory implements CharFactory {
    /**
     * Produce archer class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public Character produce(String name) {
        Character chara = new Archer(name);
        chara.setWeapon(new Bow(100, 10, "Archer Bow"));
        chara.setSpell(new Multishot(200,3));
        return chara;
    }

    @Override
    public String getType() {
        return "archer";
    }
}
