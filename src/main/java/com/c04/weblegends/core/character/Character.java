package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;
import com.c04.weblegends.core.spell.*;
import com.fasterxml.jackson.annotation.*;
import java.util.Random;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Archer.class, name = "Archer"),
        @JsonSubTypes.Type(value = Assassin.class, name = "Assassin"),
        @JsonSubTypes.Type(value = Cleric.class, name = "Cleric"),
        @JsonSubTypes.Type(value = Knight.class, name = "Knight"),
        @JsonSubTypes.Type(value = Mage.class, name = "Mage"),
        @JsonSubTypes.Type(value = Ranger.class, name = "Ranger")
})
public abstract class Character {
    protected int maxHP;
    protected int maxMana;
    protected String name;
    protected Weapon weapon;
    protected int hp;
    protected int mana;
    protected Spell spell;

    /**
     * Constructor character.
     *
     * @param name name of the player
     * @param hp   player base maximum HP
     * @param mana base maximum MP
     */
    @JsonCreator
    public Character(@JsonProperty("name") String name,
                     @JsonProperty("hp") int hp, @JsonProperty("mana") int mana) {
        this.hp = hp;
        this.mana = mana;
        this.name = name;
        this.maxHP = hp;
        this.maxMana = mana;
    }

    /**
     * Method to update player HP.
     *
     * @param hp HP that's wanted to be set
     */
    public void setHP(int hp) {
        if (hp > 0 && hp <= getMaxHP()) {
            this.hp = hp;
        } else if (hp <= 0) {
            this.hp = 0;
        } else {
            this.hp = getMaxHP();
        }
    }

    /**
     * Method to update player MP.
     *
     * @param mana MP that's wanted to be set
     */
    public void setMana(int mana) {
        if (mana > 0 && mana <= getMaxMana()) {
            this.mana = mana;
        } else if (mana <= 0) {
            this.mana = 0;
        } else {
            this.mana = getMaxMana();
        }
    }

    /**
     * Player upgrade method
     */
    public void doUpgrade() {
        Random r= new Random();
        float percent = r.nextFloat() + 1;
        int upgradeChoice = r.nextInt(2);
        switch (upgradeChoice) {
            case 0:
                this.maxMana = (int) Math.round(((float) 1 + percent) * (float) this.maxMana);
                break;
            case 1:
                this.maxHP = (int) Math.round(((float) 1 + percent) * (float) this.maxHP);
                break;
        }
    }

    public boolean useSpell() {
        if (this.spell.getMana() <= this.getMana() && this.spell.getCooldownCounter() == 0) {
            this.spell.setCooldownCounter(this.spell.getCooldown());
            this.mana = this.mana - this.spell.getMana();
            return true;
        }
        return false;
    }

    public void decCooldown() {
        if (this.spell.getCooldownCounter() > 0) {
            this.spell.setCooldownCounter(this.spell.getCooldownCounter() - 1);
        }
    }

    public int getHP() {
        return this.hp;
    }

    public int getMana() {
        return this.mana;
    }

    public int getMaxHP() {
        return this.maxHP;
    }

    public int getMaxMana() {
        return this.maxMana;
    }

    public String getName() {
        return this.name;
    }

    public abstract String getClassName();

    public Weapon getWeapon() {
        return this.weapon;
    }

    public abstract void setWeapon(Weapon weapon);

    public void setSpell(Spell spell) {
        this.spell = spell;
    }

    public int getDamage() {
        return this.weapon.getAttack();
    }

    public int getDefense() {
        return this.weapon.getDefense();
    }

    public Spell getSpell() {
        return this.spell;
    }

}