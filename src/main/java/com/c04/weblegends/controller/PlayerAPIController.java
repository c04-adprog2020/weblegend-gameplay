package com.c04.weblegends.controller;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.service.PlayerService;
import com.c04.weblegends.service.PlayerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/player-api")
public class PlayerAPIController {
    @Autowired
    private PlayerService playerService = new PlayerServiceImpl();

    /**
     * Character creation API directory.
     *
     * @param json Json that contains name and class of character
     * @return Json that contains base stat of character which is dependent on chosen class
     * @throws Exception Exception if there is no class in repo
     */
    @PostMapping(value = "/produce-hero")
    @ResponseBody
    public ResponseEntity<Character> createHero(@RequestBody String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = mapper.readValue(json, Map.class);
        Character hero = playerService.createHero(map.get("name"), map.get("className"));
        return new ResponseEntity<Character>(hero, HttpStatus.CREATED);
    }

    /**
     * Character upgrade API directory.
     *
     * @param json Json that contains player stat before upgrade
     * @return Json that contains player stat after upgrade
     * @throws Exception Exception if there is no kind of upgrade
     */
    @PostMapping(value = "/upgrade-hero")
    @ResponseBody
    public ResponseEntity<Character> upgradeHero(@RequestBody String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Character character = mapper.readValue(json, Character.class);
        Character upgradedCharacter = playerService.doPowerUp(character);
        return new ResponseEntity<Character>(upgradedCharacter, HttpStatus.OK);
    }

    /**
     * Weapon enhancement API directory.
     *
     * @param json Json that contains weapon stat before upgrade
     * @return Json that contains weapon stat after upgrade
     * @throws Exception Exception if there is kind of enhancement
     */
    @PostMapping(value = "/enhance-weapon")
    @ResponseBody
    public ResponseEntity<Character> enhanceWeapon(@RequestBody String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Character character = mapper.readValue(json, Character.class);
        Character upgradedCharacter = playerService.enhanceWeapon(character);
        return new ResponseEntity<Character>(upgradedCharacter, HttpStatus.OK);
    }

    @GetMapping(value = "/get-heroclass")
    @ResponseBody
    public ResponseEntity <Map <String,String>> getHeroClass() throws Exception {
        return new ResponseEntity<Map<String, String>>(playerService.getHeroClass(), HttpStatus.OK);
    }

}
