package com.c04.weblegends.controller;

import com.c04.weblegends.core.aicharacter.ArtificialIntelligence;
import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.service.BattleService;
import com.c04.weblegends.service.BattleServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/battle-api")
public class BattleAPIController {
    @Autowired
    private BattleService battleService = new BattleServiceImpl();

    /**
     * Enemy spawn API directory.
     *
     * @return Enemy object json
     */
    @GetMapping(value = "/spawn-enemy")
    @ResponseBody
    public ResponseEntity<ArtificialIntelligence> getEnemy(@RequestParam("level") int level) {
        return new ResponseEntity<ArtificialIntelligence>(battleService.spawnEnemy(level), HttpStatus.CREATED);
    }

    /**
     * Player attack API.
     *
     * @param jsonInput Json that contains player & hero stat
     * @return Json that contains player stat + enemy stat + battle log
     * @throws IOException Json exception
     */
    @PostMapping(value = "/player-attack")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> playerAttack(@RequestBody String jsonInput) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode parent = mapper.readTree(jsonInput);
            JsonNode playerNode = parent.get("player");
            JsonNode enemyNode = parent.get("enemy");
            Character hero = mapper.treeToValue(playerNode, Character.class);
            ArtificialIntelligence enemy = mapper.treeToValue(enemyNode, ArtificialIntelligence.class);
            ArrayList<Object> output = new ArrayList<Object>(2);
            output = battleService.playerAttack(hero, enemy);
            Map<String, Object> outputJson = new HashMap<String, Object>();
            outputJson.put("player", output.get(0));
            outputJson.put("enemy", output.get(1));
            outputJson.put("battleLog", output.get(2));
            return new ResponseEntity<Map<String, Object>>(outputJson, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        catch(IndexOutOfBoundsException e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Player skill API.
     *
     * @param jsonInput Json that contains player/hero stat
     * @return Json that contains player + enemy stat + battle log
     * @throws IOException Json exception
     */
    @PostMapping(value = "/player-skill")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> playerSkill(@RequestBody String jsonInput) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode parent = mapper.readTree(jsonInput);
            JsonNode playerNode = parent.get("player");
            JsonNode enemyNode = parent.get("enemy");
            Character hero = mapper.treeToValue(playerNode, Character.class);
            ArtificialIntelligence enemy = mapper.treeToValue(enemyNode, ArtificialIntelligence.class);
            ArrayList<Object> output = battleService.playerSpell(hero, enemy);
            Map<String, Object> outputJson = new HashMap<String, Object>();
            outputJson.put("player", output.get(0));
            outputJson.put("enemy", output.get(1));
            outputJson.put("battleLog", output.get(2));
            return new ResponseEntity<Map<String, Object>>(outputJson, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        catch(IndexOutOfBoundsException e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Enemy attack API.
     *
     * @param jsonInput Json that contains player/hero json
     * @return Json that contains player stat + enemy attack + battle log
     * @throws IOException Json exception
     */
    @PostMapping(value="/enemy-attack")
    @ResponseBody
    public ResponseEntity <Map<String,Object>> enemyAttack(@RequestBody String jsonInput) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode parent = mapper.readTree(jsonInput);
            JsonNode playerNode = parent.get("player");
            JsonNode enemyNode = parent.get("enemy");
            Character hero = mapper.treeToValue(playerNode, Character.class);
            ArtificialIntelligence enemy = mapper.treeToValue(enemyNode, ArtificialIntelligence.class);
            ArrayList<Object> output = new ArrayList<Object>();
            output = battleService.enemyAttack(enemy,hero);
            Map<String, Object> outputJson = new HashMap<String, Object>();
            outputJson.put("player", output.get(0));
            outputJson.put("enemy", output.get(1));
            outputJson.put("battleLog", output.get(2));
            return new ResponseEntity<Map<String, Object>>(outputJson, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        } catch(IndexOutOfBoundsException e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
