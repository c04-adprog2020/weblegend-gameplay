package com.c04.weblegends.controller;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.item.*;
import com.c04.weblegends.service.ItemService;
import com.c04.weblegends.service.ItemServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/item-api")
public class ItemAPIController {
    @Autowired
    private ItemService itemService = new ItemServiceImpl();

    /**
     * Item drop API directory.
     *
     * @return Random item with certain effect
     */
    @GetMapping(value = "/drop-item")
    @ResponseBody
    public ResponseEntity<Item> getItem() {
        return new ResponseEntity<Item>(itemService.getItem(), HttpStatus.CREATED);
    }

    /**
     * Item appliance to player API directory.
     *
     * @param jsonInput Json that contains player/hero & item info
     * @return Json that contains player stat after being applied to item
     * @throws IOException Json exception
     */
    @PostMapping(value = "/use-item")
    @ResponseBody
    public ResponseEntity<Character> useItem(@RequestBody String jsonInput) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode parent = mapper.readTree(jsonInput);
            JsonNode itemNode = parent.get("item");
            JsonNode characterNode = parent.get("character");
            Character hero = mapper.treeToValue(characterNode, Character.class);
            Item item = mapper.treeToValue(itemNode, Item.class);
            return new ResponseEntity<Character>(itemService.useItem(item, hero), HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


}
