package com.c04.weblegends.repo;

import com.c04.weblegends.core.character.*;
import java.util.*;

public class PlayerRepo {
    private Map<String, CharFactory> classFactories;
    private Map <String, String> heroClasses;
    /**
     * Constructor for PlayerRepo.
     */
    public PlayerRepo() {
        this.classFactories = new HashMap<String, CharFactory>();
        this.heroClasses = new HashMap <String, String> ();
        this.classFactories.put("archer", new ArcherFactory());
        this.heroClasses.put("Archer","Archer");
        this.classFactories.put("knight", new KnightFactory());
        this.heroClasses.put("Knight","Knight");
        this.classFactories.put("cleric", new ClericFactory());
        this.heroClasses.put("Cleric","Cleric");
        this.classFactories.put("mage", new MageFactory());
        this.heroClasses.put("Mage","Mage");
        this.classFactories.put("ranger", new RangerFactory());
        this.heroClasses.put("Ranger","Ranger");
        this.classFactories.put("assassin", new AssassinFactory());
        this.heroClasses.put("Assassin","Assassin");
    }

    public CharFactory getFactoryByName(String className) {
        return this.classFactories.get(className.toLowerCase());
    }

    public List<CharFactory> getFactories() {
        return new ArrayList<CharFactory>(this.classFactories.values());
    }

    public Map <String, String> getHeroClasses() {
        return this.heroClasses;
    }

    /**
     * Add new kind of factory to repository.
     *
     * @param name        : Name of the factory
     * @param charFactory : Character Factory Object
     */
    public void addFactory(String name, CharFactory charFactory) {
        this.classFactories.put(name, charFactory);
    }

}
