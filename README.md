# Web Legends Gameplay Service
master : [![pipeline status](https://gitlab.com/c04-adprog2020/weblegend-gameplay/badges/master/pipeline.svg)](https://gitlab.com/c04-adprog2020/weblegend-gameplay/-/commits/master) [![coverage report](https://gitlab.com/c04-adprog2020/weblegend-gameplay/badges/master/coverage.svg)](https://gitlab.com/c04-adprog2020/weblegend-gameplay/-/commits/master)

Web Legends adalah sebuah game online singleplayer dengan genre turn-based RPG. Game ini dibuat oleh kelompok AP C-04 dengan pembagian sebagai berikut:

1. I Made Krisna Dwitama (Combat Logic & Enemy Creation)
2. Mahardika Krisna Ihsani (Item & Powerup)
3. Ahmad Yoga Haulian Prtama (Game Stage & Flow, Frontend)
4. Sean Zheliq Urian (Scoring, Backend)
5. Muhammad Adipasha Yarzuq (Character Creation)

## Deskripsi
Sebuah API untuk mengembalikan stats pemain,musuh, dan item.
